import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsfCaseComponent } from './usf-case.component';

const routes: Routes = [{path: 'usf-case', component: UsfCaseComponent}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class UsfCaseRoutingModule {
}
