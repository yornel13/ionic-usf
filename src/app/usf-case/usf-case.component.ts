import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import * as XLSX from 'xlsx';
import { NgbCalendar, NgbDate, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseComponent } from '../core/base/BaseComponent';
import { UsfServiceService } from '../core/usf/usf-service.service';
import { AuthenticationService } from '../core';
import { LoadingService } from '../shared/components/loading-modal/loading.service';
import { RangeDate } from '../models/range.date';

@Component({
    selector: 'app-usf-case',
    templateUrl: './usf-case.component.html',
    styleUrls: ['./usf-case.component.scss']
})
export class UsfCaseComponent extends BaseComponent implements OnInit {

    @ViewChild('modalCaseDetails', { static: false }) modalCaseDetails: any;

    public status: any = ['PENDIENTE', 'DENEGADO', 'EN PROCESO', 'APROBADO'];
    public statusSelected = '';
    public nameToSearch = '';
    public numberUSF = '';
    public loadingRequest = false;
    public dateRange: any = null;
    public dataContent: any = [];

    public toShow: any[];
    public beforePage: number;
    public currentPage = 1;
    public afterPage: number;
    public displayed: number;
    public totalPages: number;
    public surplus: number;

    public selectedFirst: boolean;
    public selectedNext: boolean;
    public selectedLast: boolean;

    public totalRowCount = 0;

    hoveredDate: NgbDate | null = null;

    fromDate: NgbDate | null;
    toDate: NgbDate | null;

    caseDetail: any = undefined;
    caseDetail2: any = undefined;
    negateDescription: any = null;
    loadingDescription = false;
    casiIdSaved: any = null;

    constructor(
        private loadingService: LoadingService,
        private modalService: NgbModal,
        private calendar: NgbCalendar,
        public authenticationService: AuthenticationService,
        public usfServiceService: UsfServiceService,
        public router: Router,
        public fb: FormBuilder
    ) {
        super(authenticationService, usfServiceService, router, fb);
        this.fromDate = calendar.getToday();
        this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
    }

    ngOnInit() {
        this.selectedFirst = true;
        this.selectedNext = false;
        this.selectedLast = false;
        let mes = (new Date().getMonth() + 1).toString();
        if (new Date().getMonth() + 1 < 10) {
            mes = '0' + mes;
        }
        this.dateRange = {
            start: new Date().getFullYear() + '-01-01',
            end: new Date().getFullYear() + '-' + mes + '-' + new Date().getDate()
        };
        if (localStorage.getItem('numberCaseToSearch') !== null) {
            this.numberUSF = localStorage.getItem('numberCaseToSearch');
            this.getCasesUSF();
        }
    }

    onApply(range: RangeDate) {
        this.dateRange = range;
        this.getCasesUSF();
    }

    validaEnter(evt: any) {
        if (evt.keyCode === 13) {
            // si pisa enter
            this.getCasesUSF();
        }
    }

    goToHome() {
        this.router.navigate(['/home'], { replaceUrl: true });
    }

    isEmpty() {
        return this.nameToSearch.trim().length;
    }

    FilterByNroUsf() {
    }

    validacionOcultarRow(iten: any) {
        if (this.numberUSF.trim() !== '' && (String(iten.caseID) === String(this.numberUSF.trim())
            || String(iten.applicationId) === String(this.numberUSF.trim()))) {
            return false;
        } else if (this.numberUSF.trim() !== '' && (String(iten.caseID) !== String(this.numberUSF.trim())
            || String(iten.applicationId) !== String(this.numberUSF.trim()))) {
            return true;
        } else  if (this.statusSelected === '' && this.isEmpty() === 0) {
            return false;
        } else if (this.statusSelected !== '' && this.isEmpty() === 0 && this.statusSelected !== iten.status) {
            return true;
        } else if (this.statusSelected !== '' && this.isEmpty() === 0 && this.statusSelected === iten.status) {
            return false;
        } else if (
            this.statusSelected !== '' &&
            this.statusSelected === iten.status &&
            this.isEmpty() !== 0 &&
            iten.fullName
                .toString()
                .toLocaleLowerCase()
                .includes(this.nameToSearch.toString().toLocaleLowerCase())
        ) {
            return false;
        } else {
            if (
                this.isEmpty() !== 0 &&
                iten.fullName
                    .toString()
                    .toLocaleLowerCase()
                    .includes(this.nameToSearch.toString().toLocaleLowerCase())
            ) {
                return false;
            } else {
                return true;
            }
        }
    }

    exportTo() {
        const dataArray: any = [['Nro de USF', 'BAN', 'Fecha Registro de USF', ' Nombre y Apellido Cliente', 'Estatus']];
        this.dataContent.forEach((caso: any) => {
            dataArray.push([caso.caseID, caso.ban, caso.date, caso.fullName, caso.status]);
        });

        /* generate worksheet */
        const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(dataArray);

        /* generate workbook and add the worksheet */
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

        /* save to file */
        // XLSX.writeFile(wb, 'CasesUSF.xlsx');

        // TEST
        // The exported read and readFile functions accept an options argument:
        // {password: '123', WTF: true}
        /**/
        XLSX.readFile(XLSX.writeFile(wb, 'CasesUSF.xlsx'), { password: '123', WTF: true });
    }

    getCasesUSF() {

        this.loadingService.startLoading('Espere un momento mientras es procesada la información...');
        const data = {
            method: 'getCasesWithFiltersMcapi',
            DateFrom: this.dateRange.start,
            DateTo: this.dateRange.end,
            pageNo: this.paginationBox.currentPage,
            pageSize: this.paginationBox.itemsPerPage,
            caseID: /^([0-9])*$/.test(this.numberUSF.toString()) ? this.numberUSF : '',
            Status: this.statusSelected,
            userID: this.authenticationService.credentials.userid,
            applicationId: /^([0-9])*$/.test(this.numberUSF.toString()) ? '' : this.numberUSF
        };
        return this.usfServiceService.doAction(data).subscribe((dt: any) => {
            if (!dt.HasError) {
                this.dataContent = [];
                this.toShow = [];
                this.totalRowCount = dt.totalRowCount;
                this.displayed = 10;

                this.totalPages = Math.trunc(this.totalRowCount / 10);
                this.surplus = this.totalRowCount % 10;
                if (this.surplus !== 0) {
                    this.totalPages = this.totalPages + 1;
                }

                if (this.totalRowCount !== 0 && this.totalRowCount > 10 && dt.customercases.length <= 10) {
                    this.paginationBox.itemsPerPage = this.totalPages * 10;

                    this.getCasesUSF();
                } else {
                    dt.customercases.forEach((caso: any) => {
                        const dateTemp = new Date(caso.DTS_CREATED);
                        const dd: number = dateTemp.getDate();
                        let ddTxt = '';
                        const mm: number = dateTemp.getMonth() + 1;
                        let mmTxt = '';

                        if (dd < 10) {
                            ddTxt = '0' + dd.toString();
                        } else {
                            ddTxt = dd.toString();
                        }

                        if (mm < 10) {
                            mmTxt = '0' + mm.toString();
                        } else {
                            mmTxt = mm.toString();
                        }

                        this.dataContent.push({
                            caseID: caso.USF_CASEID,
                            ban: caso.ACCOUNT_NUMBER,
                            date: mmTxt + '/' + ddTxt + '/' + dateTemp.getFullYear(),
                            fullName: caso.CUSTOMER_NAME + ' ' + caso.CUSTOMER_LAST,
                            status: caso.CASE_STATUS,
                            applicationId: caso.applicationId,
                            eligibilityExpirationDate: caso.eligibilityExpirationDate
                        });
                    });
                    this.firstPage();
                    this.getPage(1);
                    this.loadingRequest = false;
                    this.loadingService.stopLoading();
                }
            }
        });
    }

    // funciones de paginado
    nextPage() {
        if (this.totalRowCount > 10) {
            if (this.currentPage < this.totalPages - 3) {
                this.currentPage = this.currentPage + 3;
                this.getPage(1);
            } else {
                // alert('ya estas es la ultima pagina y no se puede avanzar');
            }
        }
    }

    previousPage() {
        if (this.currentPage > 2) {
            this.currentPage = this.currentPage - 3;
            this.getPage(1);
        } else {
            this.currentPage = 1;
            // alert('ya estas es la primera pagina y no se puede retroceder');
        }
    }

    firstPage() {
        this.currentPage = 1;
        this.getPage(1);
    }

    lastPage() {
        if (this.totalRowCount > 10) {
            this.currentPage = this.totalPages - 2;
            this.getPage(1);
        }
    }

    getPage(x: number) {
        // array auxiliar:
        this.toShow = [];
        // parametro de entrada de la funcion
        const param = x - 1;
        // objeto con variables de control de paginado
        const pages = {
            b: 0,
            item_final: 0,
            item_inicial: 0,
            mostrar: [1, 2] // cualquier cosa despues de cambia
        };

        pages.b = this.currentPage;
        pages.b = pages.b + param;
        pages.item_final = pages.b * 10;
        pages.item_inicial = pages.item_final - 9;

        // para el pie de la tabla:
        let cont = 0;
        const bar = [];
        for (let i = pages.item_inicial - 1, l = pages.item_final; i < l; i++) {
            if (this.dataContent[i]) {
                this.toShow[cont] = this.dataContent[i];
            }
            cont = cont + 1;
        }

        if (this.toShow.length === 10) {
            this.displayed = pages.item_final;
        } else {
            this.displayed = pages.item_final - (10 - this.toShow.length);
        }

        // estilo dinamico:
        switch (x) {
            case 1:
                this.selectedFirst = true;
                this.selectedNext = false;
                this.selectedLast = false;
                break;
            case 2:
                this.selectedFirst = false;
                this.selectedNext = true;
                this.selectedLast = false;
                break;
            default:
                this.selectedFirst = false;
                this.selectedNext = false;
                this.selectedLast = true;
                break;
        }

        return pages;
    }

    showDetails(caseID: any) {
        this.casiIdSaved = null;
        this.negateDescription = null;
        const data = {
            method: 'getCasesModifyBackendId',
            caseID,
            UserID: this.authenticationService.credentials.userid
        };
        this.loadingService.startLoading();
        this.usfServiceService.doAction(data).subscribe((resp: any) => {
            this.loadingService.stopLoading();

            if (resp.dataBacken.length > 0) {
                this.caseDetail = resp.dataBacken[0];
                this.caseDetail2 = resp.dataBacken[0];
                this.openModal(this.modalCaseDetails);
            }

            const data2 = {
                method: 'getCasesdeniedMcapi',
                caseID,
                // caseID: 1399,
                UserID: this.authenticationService.credentials.userid
                // UserID: 99
            };
            this.loadingDescription = true;
            this.usfServiceService.doAction(data2).subscribe((resp2: any) => {
                this.loadingDescription = false;
                if (resp2.data.length !== 0) {
                    // @ts-ignore
                    this.negateDescription = resp2.data.map(x => x.DENIED_DESCRIPCION).join(', ');
                }
            });

            const data3 = {
                method: 'eligibilitycheckStatusMcapi',
                eligibilityCheckId: this.caseDetail.eligibilityCheckId,
                repId: ''
            };
            this.usfServiceService.doAction(data3).subscribe((resp3: any) => {
                this.loadingDescription = false;
                if (resp3.activeSubscriber === 'N' && resp3.status === 'COMPLETE' && this.caseDetail.ACCOUNT_NUMBER === '') {
                    this.casiIdSaved = caseID;
                }
            });
        });
    }

    continueToCase() {
        const dataObjectAddress = [];
        dataObjectAddress.push({
            CUSTOMERNAME: this.caseDetail.CUSTOMER_NAME,
            CUSTOMERLASTNAME: this.caseDetail.CUSTOMER_LAST,
            CUSTOMERADDRESS: this.caseDetail.ADDRESS_1 + ' ' +
                this.caseDetail.ADDRESS_2 + ' ' +
                this.caseDetail.CITY + ' ' +
                this.caseDetail.STATE + ' ' +
                this.caseDetail.ZIPCODE,
            SUGGESTADDRESS: this.caseDetail2.ADDRESS_1 + ' ' +
                this.caseDetail2.ADDRESS_2 + ' ' +
                this.caseDetail2.CITY + ' ' +
                this.caseDetail2.STATE + ' ' +
                this.caseDetail2.ZIPCODE,
            SSN: this.caseDetail.CUSTOMER_SSN,
            contactNumber1: this.caseDetail.PHONE_1,
            contactNumber2: this.caseDetail2.PHONE_1
        });

        sessionStorage.setItem('validateSSNData', '{"CASENUMBER":' + this.casiIdSaved + '}');
        sessionStorage.setItem('ssn', this.caseDetail.CUSTOMER_SSNMAX);
        sessionStorage.setItem('dataObjectAddress', JSON.stringify(dataObjectAddress));
        this.router.navigate(['/universal-service/document-digitalization'], { replaceUrl: true }).then();
    }

    openModal(content: any) {
        this.modalService.open(
            content,
            {
                ariaLabelledBy: 'modal-basic-title',
                centered: true,
                size: 'lg',
                backdrop: 'static',
                scrollable: false
            }).result.then();
    }
}
