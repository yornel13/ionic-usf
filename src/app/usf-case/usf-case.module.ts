import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsfCaseComponent } from './usf-case.component';
import { UsfCaseRoutingModule } from './usf-case-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared/shared.module';
import { CapitalizePipe } from '../pipes/capitalize.pipe';
import { IonicModule } from '@ionic/angular';

@NgModule({
    declarations: [
        UsfCaseComponent,
        CapitalizePipe
    ],
    imports: [
        IonicModule,
        CommonModule,
        UsfCaseRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule
    ]
})
export class UsfCaseModule {
}
