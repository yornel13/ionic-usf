export interface HttpCapture {
    request: any;
    response: any;
    url: string;
    status: number;
    method: string;
    platform: any[];
    requestTime: Date;
    responseTime: Date;
    duration: number; // seconds
    logged: boolean;
    webVersion: string;
    appVersionNumber?: any;
    appVersionCode?: any;
    user?: {
        id: number,
        name: string,
        lastname: string
        username: string,
        email: string,
        role: string
    };
}
