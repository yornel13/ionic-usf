export interface RangeDate {
    start?: string;
    end?: string;
}
