import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '../core';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [CommonModule, CoreModule, HomeRoutingModule, SharedModule, FormsModule, ReactiveFormsModule],
  declarations: [HomeComponent]
})
export class HomeModule {}
