import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseComponent } from '../core/base/BaseComponent';
import { LoadingService } from '../shared/components/loading-modal/loading.service';
import { AuthenticationService } from '../core';
import { UsfServiceService } from '../core/usf/usf-service.service';
import InputValidationUtil from '../util/input.validation.util';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent extends BaseComponent implements OnInit {

    @ViewChild('modalProcessValidation', { static: false }) modalProcessValidation: any;

    numberCase = '';
    applicationId = '';
    isLoading: boolean;
    permisions = true;
    processValidation = false;
    checkTerms = false;
    someErrorValidation = false;
    errorObject: any = null;
    complete = false;
    firstTime = true;
    firstTimeNumberCase = true;

    constructor(
        private loadingService: LoadingService,
        private modalService: NgbModal,
        public authenticationService: AuthenticationService,
        public usfServiceService: UsfServiceService,
        public router: Router,
        public fb: FormBuilder
    ) {
        super(authenticationService, usfServiceService, router, fb);
        this.usfServiceService.setValue('validateSSNData', null);
        this.usfServiceService.setValue('dataObjectAddress', null);
        this.usfServiceService.setValue('ssn', null);
        this.usfServiceService.setValue('requiredDocuments', null);
        this.usfServiceService.setValue('dataAgencyMoneySelection', null);
    }

    ngOnInit() {
        this.isLoading = true;
        this.numberCase = '';
        this.getRol();

        localStorage.setItem('personalData', null);
    }

    goToUniversalService() {
        this.checkTerms = false;

        if (this.validateForm()) {
            this.processValidation = true;
            this.openModal(this.modalProcessValidation);
        }
    }

    gotoUsfCase() {
        localStorage.setItem('numberCaseToSearch', this.numberCase);
        this.router.navigate(['/usf-case'], { replaceUrl: true }).then();
    }

    getRol() {
        const data = this.usfServiceService.getValue('credentials');
        let credentials = '';
        credentials = data.RoleName;
        if (credentials === 'USF_SUP_V_Indirectas') {
            this.permisions = false;
        }
    }

    validateForm() {
        let go = false;
        if (this.applicationId.length === 0) {
            go = true;
        } else {
            if (this.applicationId.length === 12) {
                go = true;
            } else {
                alert('El ID de la aplicación es incorrecto, intente nuevamente');
                go = false;
            }
        }
        return go;
    }

    async goToUniversalServiceV2() {
        if (this.checkTerms === true) {
            this.usfServiceService.setValue('applicationId', this.applicationId);
            try {
                this.loadingService.startLoading('Cargando, por favor espere un momento...');
                const resp = await this.usfServiceService.doAction({ method: 'GetListAgentMcapi' }).toPromise();
                this.loadingService.stopLoading();
                const result = resp.data;

                // @ts-ignore
                result.forEach(item => {
                    item.cod_id = item.cod_id.replace(/ /g, '');
                });

                this.usfServiceService.setValue('agencies', resp.data);
                this.goTo('/universal-service/personal-dates');
            } catch (e) {
                this.loadingService.stopLoading();
            }
        }
        // TODO: mover a donde se valida el national verifier (despues de validar la direccion)
    }

    setFormatInputAppId() {
        if (this.applicationId.length === 11 && this.complete === false) {
            this.applicationId =
                String(this.applicationId).substr(0, 6) +
                '-' +
                String(this.applicationId).substr(6, this.applicationId.length);
            this.complete = true;
            this.firstTime = false;

        } else {
            if (this.firstTime === false && this.applicationId.length < 6) {
                this.complete = false;
                this.firstTime = true;
            }
        }
        return this.applicationId;
    }

    setFormatInputCase() {
        const format = InputValidationUtil.setFormatInputCase(this.numberCase, 'numberCase', this.complete, this.firstTimeNumberCase);
        this.numberCase = format.inputValue;
        this.complete = format.complete;
        this.firstTimeNumberCase = format.firstTimeNumberCase;
    }

    validateFormNumberCase() {
        let go = true;
        if (!/^([0-9])*$/.test(this.numberCase.toString())) {
            if (this.numberCase.length === 0) {
                go = true;
            } else {
                if (this.numberCase.length === 12) {
                    go = true;
                } else {
                    alert('El ID de la aplicación es incorrecto, intente nuevamente');
                    go = false;
                }
            }
        }
        return go;
    }

    openModal(content: any) {
        this.modalService.open(
            content,
            {
                ariaLabelledBy: 'modal-basic-title',
                centered: true,
                size: 'lg',
                backdrop: 'static',
                scrollable: true
            }).result.then();
    }
}

