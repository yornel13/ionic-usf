export interface PlatformInfo {
    android: boolean;
    iphone: boolean;
    ipad: boolean;
    ios: boolean;
    desktop: boolean;
}

export default class Util {

    static getPlatformInfo(): PlatformInfo {
        return {
            android: (navigator.userAgent.toLowerCase().indexOf('android') > -1),
            iphone: (navigator.userAgent.toLowerCase().indexOf('iphone') > -1),
            ipad: (navigator.userAgent.toLowerCase().indexOf('ipad') > -1),
            ios:  (navigator.userAgent.toLowerCase().indexOf('iphone') > -1)
                || (navigator.userAgent.toLowerCase().indexOf('ipad') > -1),
            desktop: !window.hasOwnProperty('cordova')
        };
    }
}
