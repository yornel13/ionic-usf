export default class InputValidationUtil {

    static checkCharactersOnlyOnChange(event: any) {
        const value = event.target.value;
        const newValue = value.slice(-1);
        const isValid = /^[a-zA-Z ]+$/.test(newValue);
        if (newValue && !isValid) {
            event.target.value = value.replace(newValue, '');
        }
    }

    static checkCharactersOnlyOnBlur(event: any) {
        const value = event.target.value;
        event.target.value = value.replace(/[^a-zA-Z ]/g, '').trim();
    }

    static checkNumbersOnlyOnChange(event: any) {
        const value = event.target.value;
        const newValue = value.slice(-1);
        const isValid = /[0-9]/.test(newValue);
        if (newValue && !isValid) {
            event.target.value = value.replace(newValue, '');
        }
    }

    static checkNumbersOnly(event: any): boolean {
        const charCode = event.which ? event.which : event.keyCode;

        if (charCode === 8) {
            return true;
        } else {
            const patron = /[0-9]/;
            return patron.test(String.fromCharCode(charCode));
        }
    }

    static checkCharactersOnly(event: any): boolean {
        const charCode = event.which ? event.which : event.keyCode;

        if (charCode === 8 || charCode === 32) {
            return true;
        } else {
            const patron = /[A-Za-z]/;
            return patron.test(String.fromCharCode(charCode));
        }
    }

    static setFormatInputCase(inputValue: string, type: string, param: any, param2: any) {
        const formatControl = {
            complete: param,
            firstTimeNumberCase: param2,
            inputValue,
            valueSSN: '',
            checkSSN: false
        };
        if (type === 'numberCase') {
            if (formatControl.inputValue.length === 11 && formatControl.complete === false) {
                formatControl.inputValue =
                    String(inputValue).substr(0, 6) + '-' + String(inputValue).substr(6, inputValue.length);
                formatControl.complete = true;
                formatControl.firstTimeNumberCase = false;
            } else {
                if (formatControl.firstTimeNumberCase === false && formatControl.inputValue.length < 6) {
                    formatControl.complete = false;
                    formatControl.firstTimeNumberCase = true;
                }
            }
            return formatControl;
        } else {
            // ssn procedimiento
            const input: string = inputValue;
            if (param2) {
                formatControl.valueSSN = input.replace(/-/g, '');
                param = String(param).substr(0, 3) + '-' + String(param).substr(3, 2) + '-' + String(param).substr(5, 4);
                formatControl.checkSSN = true;
            }

            if (input.length > 1 && String(input).substr(input.length - 1, 1) !== 'X') {
                formatControl.valueSSN += String(input.substr(input.length - 1, 1));
            } else if (input !== 'X' && input !== 'XXX-XX-XXXX') {
                formatControl.valueSSN = input;
            }

            if (input.length === 4 || input.length === 7) {
                formatControl.valueSSN =
                    input.substr(0, input.length - 1) +
                    (input[input.length - 1] === '-' ? '' : '-') +
                    input.substr(input.length - 1, input.length);
            } else {
                formatControl.valueSSN = input;
            }
            return formatControl;
        }
    }

    static validateRightSSN(socialSecure: string, valueSSN: string) {
        return socialSecure.length === 11 && parseInt(String(valueSSN).replace(/-/g, ''), 10) >= 9999999;
    }
}
