import { IonicModule } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { environment } from 'src/environments/environment';
import { CoreModule } from 'src/app/core';
import { HomeModule } from './home/home.module';
import { LoginModule } from './login/login.module';
import { UniversalServiceModule } from './universal-service/universal-service.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { UsfCaseModule } from './usf-case/usf-case.module';
import { SharedModule } from './shared/shared.module';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

@NgModule({
    imports: [
        IonicModule.forRoot(),
        BrowserModule,
        ServiceWorkerModule.register('./ngsw-worker.js', {enabled: environment.production}),
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        NgbModule,
        CoreModule,
        HomeModule,
        SharedModule,
        LoginModule,
        UniversalServiceModule,
        UsfCaseModule,
        AppRoutingModule
    ],
    declarations: [AppComponent],
    providers: [StatusBar, SplashScreen, Camera, AppVersion, { provide: LocationStrategy, useClass: HashLocationStrategy }],

    bootstrap: [AppComponent]
})
export class AppModule {
}
