import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { BaseRouter } from './BaseRouter';
import { AuthenticationService } from '..';
import { UsfServiceService } from '../usf/usf-service.service';
import Util from '../../util/util';
import InputValidationUtil from '../../util/input.validation.util';

declare let alertify: any;

export interface ValidateSSNData {
    data: ValidateSSNDataContent[];
    dataObject: ValidateSSNDataContent[];
    response: string;
    CASE_NUMBER: number;
}

export interface ValidateSSNDataContent {
    accountType: string;
    address: string;
    ban: string;
    lifelineActivationDate: string;
    name: string;
    ssn: string;
    subscriberNumber: string;
    phone1: string;
    CASE_NUMBER: string;
}

export interface DataObjectAddress {
    CASE_NUMBER: string;
    CUSTOMER_ADDRESS: string;
    CUSTOMER_LASTNAME: string;
    CUSTOMER_NAME: string;
    DOB: string;
    SSN: string;
    SUGGEST_ADDRESS: string;
    contactNumber1: string;
    contactNumber2: string;
}

export interface PeopleData {
    number: number;
    money: string;
}

export interface DataAgencyMoneySelection {
    agency: string;
    ldiRestriction: boolean;
    peopleDataSelectedNumber: number;
    peopleDataSelected: PeopleData;
    earningsValidation: boolean;
    lifelineProgramInscription: boolean;
    acceptanceTerms: boolean;
}

export class BaseComponent extends BaseRouter {

    protected alertify = alertify;

    protected isWeb = !Util.getPlatformInfo().android && !Util.getPlatformInfo().ios;

    protected form: FormGroup;
    protected validateSSNData: ValidateSSNData;
    protected dataObjectAddress: DataObjectAddress[];
    protected moment = moment;

    protected loadingRequest = false;
    protected dateRange: any = null;
    protected dataContent: any = [];
    protected toShow: any[];
    protected currentPage = 1;
    protected displayed: number;
    protected totalPages: number;
    protected surplus: number;
    protected selectedFirst: boolean;
    protected selectedNext: boolean;
    protected selectedLast: boolean;
    protected totalRowCount = 0;
    protected paginationBox = {
        currentPage: 1,
        itemsPerPage: 10
    };
    protected numberUSF = '';
    protected statusSelected = '';

    protected checkCharactersOnlyOnChange = (event: any) => InputValidationUtil.checkCharactersOnlyOnChange(event);
    protected checkCharactersOnlyOnBlur = (event: any) => InputValidationUtil.checkCharactersOnlyOnBlur(event);
    protected checkNumbersOnlyOnChange = (event: any) => InputValidationUtil.checkNumbersOnlyOnChange(event);
    protected checkNumbersOnly = (event: any): boolean => InputValidationUtil.checkNumbersOnly(event);

    constructor(
        public authenticationService: AuthenticationService,
        public usfServiceService: UsfServiceService,
        public router: Router,
        public fb: FormBuilder
    ) {
        super(router);
        authenticationService.validaSessionActive();
        this.authenticationService.getCredentials().timeLogin = new Date();
        this.authenticationService.setCredentials(this.authenticationService.getCredentials());
        this.validateSSNData = this.usfServiceService.getValue('validateSSNData');
        this.dataObjectAddress = this.usfServiceService.getValue('dataObjectAddress');
    }

    public goToHome() {
        this.router.navigate(['/home'], {replaceUrl: true}).then();
    }

    getFormatDateCustom = (date: string) => this.moment(new Date(date)).format('MM/DD/YYYY');

    protected convertAccents(oration: string) {
        const accents = [
            ['&aacute;', 'á'],
            ['&eacute;', 'é'],
            ['&iacute;', 'í'],
            ['&oacute;', 'ó'],
            ['&uacute;', 'ú'],
            ['&ntilde;', 'ñ'],
            ['&Aacute;', 'Á'],
            ['&Eacute;', 'É'],
            ['&Iacute;', 'Í'],
            ['&Oacute;', 'Ó'],
            ['&Uacute;', 'Ú'],
            ['&Ntilde;', 'Ñ']
        ];
        accents.map((accent: any) => {
            oration = oration.replace(accent[0], accent[1]);
        });
        return oration;
    }

    delay(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    /**
     * Externalizando filter case:
     */

    getCasesUSF() {
        this.loadingRequest = true;
        const data = {
            method: 'getCasesWithFiltersMcapi',
            DateFrom: this.dateRange.start,
            DateTo: this.dateRange.end,
            pageNo: this.paginationBox.currentPage,
            pageSize: this.paginationBox.itemsPerPage,
            caseID: /^([0-9])*$/.test(this.numberUSF.toString()) ? this.numberUSF : '',
            Status: this.statusSelected,
            userID: this.authenticationService.credentials.userid,
            applicationId: /^([0-9])*$/.test(this.numberUSF.toString()) ? '' : this.numberUSF
        };
        //    this.http.post<any>(constants.URL_CASES, data, { observe: 'response' }).subscribe((dt: any) => {
        return this.usfServiceService.doAction(data).subscribe((dt: any) => {
            if (!dt.HasError) {
                this.dataContent = [];
                this.toShow = [];
                this.totalRowCount = dt.totalRowCount;
                this.displayed = 10;
                this.totalPages = Math.trunc(this.totalRowCount / 10);
                this.surplus = this.totalRowCount % 10;
                if (this.surplus !== 0) {
                    this.totalPages = this.totalPages + 1;
                }
                if (this.totalRowCount !== 0 && this.totalRowCount > 10 && dt.customercases.length <= 10) {
                    this.paginationBox.itemsPerPage = this.totalPages * 10;

                    this.getCasesUSF();
                } else {
                    dt.customercases.forEach((caso: any) => {
                        const dateTemp = new Date(caso.DTS_CREATED);
                        const dd: number = dateTemp.getDate();
                        let ddTxt = '';
                        const mm: number = dateTemp.getMonth() + 1;
                        let mmTxt = '';

                        if (dd < 10) {
                            ddTxt = '0' + dd.toString();
                        } else {
                            ddTxt = dd.toString();
                        }

                        if (mm < 10) {
                            mmTxt = '0' + mm.toString();
                        } else {
                            mmTxt = mm.toString();
                        }
                        this.dataContent.push({
                            caseID: caso.USF_CASEID,
                            ban: caso.ACCOUNT_NUMBER,
                            date: mmTxt + '/' + ddTxt + '/' + dateTemp.getFullYear(),
                            fullName: caso.CUSTOMER_NAME + ' ' + caso.CUSTOMER_LAST,
                            status: caso.CASE_STATUS,
                            applicationId: caso.applicationId,
                            eligibilityExpirationDate: caso.eligibilityExpirationDate
                        });
                    });
                    this.firstPage();
                    this.getPage(1);
                    this.loadingRequest = false;
                }
            }
        });
    }

    nextPage() {
        if (this.totalRowCount > 10) {
            if (this.currentPage < this.totalPages - 3) {
                this.currentPage = this.currentPage + 3;
                this.getPage(1);
            } else {
                // nothing to do
            }
        }
    }

    previousPage() {
        if (this.currentPage > 2) {
            this.currentPage = this.currentPage - 3;
            this.getPage(1);
        } else {
            this.currentPage = 1;
        }
    }

    firstPage() {
        this.currentPage = 1;
        this.getPage(1);
    }

    lastPage() {
        if (this.totalRowCount > 10) {
            this.currentPage = this.totalPages - 2;
            this.getPage(1);
        }
    }

    getPage(x: number) {
        this.toShow = [];
        const param = x - 1;
        const pages = {
            b: 0,
            item_final: 0,
            item_inicial: 0,
            mostrar: [0]
        };

        pages.b = this.currentPage;
        pages.b = pages.b + param;
        pages.item_final = pages.b * 10;
        pages.item_inicial = pages.item_final - 9;
        let cont = 0;
        const bar = [];
        for (let i = pages.item_inicial - 1, l = pages.item_final; i < l; i++) {
            if (this.dataContent[i]) {
                this.toShow[cont] = this.dataContent[i];
            }
            cont = cont + 1;
        }

        if (this.toShow.length === 10) {
            this.displayed = pages.item_final;
        } else {
            this.displayed = pages.item_final - (10 - this.toShow.length);
        }

        switch (x) {
            case 1:
                this.selectedFirst = true;
                this.selectedNext = false;
                this.selectedLast = false;
                break;
            case 2:
                this.selectedFirst = false;
                this.selectedNext = true;
                this.selectedLast = false;
                break;
            default:
                this.selectedFirst = false;
                this.selectedNext = false;
                this.selectedLast = true;
                break;
        }

        return pages;
    }
}
