import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { USFApiResponse } from '../../models/usf.api.response';
import { environment } from '../../../environments/environment';
import { HttpCapture } from '../../models/http.capture';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { AppVersion } from '@ionic-native/app-version/ngx';

declare let alertify: any;

const MAX_WAIT_TIME_CASE = 180 * 1000;
const GLOBAL_DELAY = 1500;

@Injectable()
export class UsfServiceService {

    alertify = alertify;

    constructor(
        private http: HttpClient,
        private platform: Platform,
        private router: Router,
        private appVersion: AppVersion) {
    }

    getValue(key: string): any | null {
        let data: any;
        data = JSON.parse(sessionStorage.getItem(key));
        return data;
    }

    setValue(key: string, data: any) {
        if (data) {
            sessionStorage.setItem(key, JSON.stringify(data));
        } else {
            sessionStorage.removeItem(key);
        }
    }

    doAction(data: any): Observable<any> {
        return new Observable(subscriber => {
            const credentials: any = this.getValue('credentials');
            if (credentials) {
                credentials.timeLogin = new Date();
                this.setValue('credentials', credentials);
            }
            const url = environment.MOCK_API ? environment.MOCK_API_PATH : environment.URL + environment.API_PATH;
            this.apiRequest(url, data)
                .then(value => {
                    setTimeout(() => {
                        subscriber.next(value);
                        subscriber.complete();
                    }, GLOBAL_DELAY);
                });
            if (data.method === 'subscriberVerificationMcapi') {
                setTimeout(() => {
                    subscriber.next(this.getDefaultErrorResponse());
                    subscriber.complete();
                }, MAX_WAIT_TIME_CASE);
            }
        });
    }

    private apiRequest(url: string, data: any) {
        return new Promise(async resolve => {
            const requestTime = new Date();
            try {
                const response: HttpResponse<USFApiResponse> = await this.http.post<any>(url, data,
                    {observe: 'response'}).toPromise();
                this.printCapture(url, data, requestTime, response.body, new Date(), response.status);
                resolve(response.body);
            } catch (e) {
                resolve(this.getDefaultErrorResponse());
                setTimeout(() => {
                    if (e instanceof HttpErrorResponse) {
                        if (e.status === 404 || e.status === 500) {
                            this.alertify.alert(
                                'Aviso',
                                'Ha ocurrido un error, por favor intente mas tarde.'
                            );
                        } else if (e.status === 401) {
                            this.alertify.alert(
                                'Aviso',
                                'Estimado cliente su sesión ha expirado.',
                                () => {
                                    this.router.navigate(['/login'], { replaceUrl: true }).then();
                                }
                            );
                        } else {
                            this.alertify.alert(
                                'Aviso',
                                'Ha ocurrido un error, por favor verifique su conexion a internet.'
                            );
                        }
                        if (e.status > 0) { // Capture only if internet connection exits
                            this.printCapture(url, data, requestTime, e.message, new Date(), e.status);
                        }
                    } else {
                        this.alertify.alert(
                            'Aviso',
                            'Ha ocurrido un error, por favor verifique su conexion a internet.'
                        );
                    }
                }, GLOBAL_DELAY);
            }
        });
    }

    private getDefaultErrorResponse(): USFApiResponse {
        return {HasError: true};
    }

    private async printCapture(url: string, request: any, requestTime: Date, response: any, responseTime: Date, status: number) {
        try {
            const savedCredentials: any = this.getValue('credentials');
            const httpCapture: HttpCapture = {
                url,
                request,
                response,
                status,
                method: request.method,
                platform: this.platform.platforms(),
                requestTime,
                responseTime,
                duration: Math.round((responseTime.getTime() - requestTime.getTime()) / 1000),
                logged: !!savedCredentials,
                webVersion: environment.VERSION
            };
            if (savedCredentials) {
                httpCapture.user = {
                    id: savedCredentials.userid,
                    name: savedCredentials.Name,
                    lastname: savedCredentials.LastName,
                    username: savedCredentials.UserName,
                    email: savedCredentials.Email,
                    role: savedCredentials.RoleName,
                };
            }
            if (!this.platform.is('desktop')) {
                await this.setupAppVersion(httpCapture);
            }
            this.saveLog(httpCapture);
        } catch (e) { console.log(e); }
    }

    private async saveLog(httpCapture: HttpCapture) {
        this.http.post<any>(environment.API_LOG, httpCapture).toPromise()
            .then()
            .catch(() => console.error('save log failed'));
    }

    private async setupAppVersion(httpCapture: HttpCapture) {
        try {
            httpCapture.appVersionNumber = await this.appVersion.getVersionNumber();
            httpCapture.appVersionCode = await this.appVersion.getVersionCode();
        } catch (e) {
            return;
        }
    }

    public async getAppVersion() {
        let version = '';
        try {
            if (!this.platform.is('desktop')) {
                const appVersionNumber = await this.appVersion.getVersionNumber();
                const appVersionCode = await this.appVersion.getVersionCode();
                version = `v${appVersionNumber} (${appVersionCode})`;
            } else {
                version = `v${environment.VERSION}`;
            }
            return version;
        } catch (e) {
            version = `v${environment.VERSION}`;
            return version;
        }
    }
}
