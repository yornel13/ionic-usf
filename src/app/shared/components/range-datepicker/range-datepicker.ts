import { NgbCalendar, NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { Component, EventEmitter, Output } from '@angular/core';
import { RangeDate } from '../../../models/range.date';

@Component({
    selector: 'ngb-range-datepicker',
    templateUrl: './range-datepicker.html',
    styleUrls: ['./range-datepicker.scss']
})
// tslint:disable-next-line:component-class-suffix
export class NgbRangeDatepicker {

    @Output() apply: EventEmitter<RangeDate> = new EventEmitter<RangeDate>();

    hoveredDate: NgbDate | null = null;

    fromDate: NgbDate | null;
    toDate: NgbDate | null;

    constructor(private calendar: NgbCalendar) {
        this.onClear();
    }

    onDateSelection(date: NgbDate) {
        if (!this.fromDate && !this.toDate) {
            this.fromDate = date;
        } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
            this.toDate = date;
        } else {
            this.toDate = null;
            this.fromDate = date;
        }
    }

    isHovered(date: NgbDate) {
        return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
    }

    isInside(date: NgbDate) {
        return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
    }

    isRange(date: NgbDate) {
        return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
    }

    toModel(): string | null {
        if (this.fromDate && this.toDate) {
            return this.formatDate(this.fromDate) + ' - ' + this.formatDate(this.toDate);
        } else {
            return null;
        }
    }

    formatDate(date: NgbDate) {
        return this.getMonth(date.month) + ' ' + date.day + ', ' + date.year;
    }

    onApply() {
        const range: RangeDate = {
            start: this.fromDate.year + '-'
                + (this.fromDate.month < 10 ? '0' : '') + this.fromDate.month + '-'
                + (this.fromDate.day < 10 ? '0' : '') + this.fromDate.day,
            end: this.toDate.year + '-'
                + (this.toDate.month < 10 ? '0' : '') + this.toDate.month + '-'
                + (this.toDate.day < 10 ? '0' : '') + this.toDate.day,
        };
        this.apply.emit(range);
    }

    onClear() {
        this.fromDate = NgbDate.from({year: this.calendar.getToday().year, month: 1, day: 1});
        this.toDate = this.calendar.getToday();
    }

    getMonth(month: number) {
        switch (month) {
            case 1:
                return 'Enero';
            case 2:
                return 'Febrero';
            case 3:
                return 'Marzo';
            case 4:
                return 'Abril';
            case 5:
                return 'Mayo';
            case 6:
                return 'Junio';
            case 7:
                return 'Julio';
            case 8:
                return 'Agosto';
            case 9:
                return 'Septiembre';
            case 10:
                return 'Octubre';
            case 11:
                return 'Noviembre';
            case 12:
                return 'Diciembre';
            default:
                return '';
        }
    }
}








