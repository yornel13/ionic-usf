import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { LoadingModal } from './loading.modal';

@Injectable({
    providedIn: 'root'
})
export class LoadingService {

    private modalRef: NgbModalRef;

    constructor(private modalService: NgbModal) { }

    // tslint:disable-next-line:variable-name
    private _loading = false;

    get loading(): boolean {
        return this._loading;
    }

    set loading(value) {
        this._loading = value;
    }

    startLoading(message?: string | undefined) {
        this.loading = true;
        this.openModal(message);
    }

    stopLoading() {
        this.loading = false;
        this.modalService.dismissAll();
    }

    private openModal(message: string) {
        this.modalRef = this.modalService.open(LoadingModal, {
            ariaLabelledBy: 'modal-basic-title',
            centered: true, size: 'lg', backdrop: 'static'
        });
        if (message) {
            this.modalRef.componentInstance.message = message;
        }
        this.modalRef.result.then((result: any) => {
            // this.closeResult = `Closed with: ${result}`;
        }, (reason: any) => {
            // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }
}
