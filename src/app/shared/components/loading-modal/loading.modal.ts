import { Component } from '@angular/core';

@Component({
    selector: 'modal-loading',
    templateUrl: './loading.modal.html',
    styleUrls: ['./loading.modal.scss']
})
// tslint:disable-next-line:component-class-suffix
export class LoadingModal {

    message = 'Cargando, porfavor espere...';

    constructor() {}
}
