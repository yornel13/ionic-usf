import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BaseRouter } from '../../../core/base/BaseRouter';
import { AuthenticationService } from '../../../core';
import { UsfServiceService } from '../../../core/usf/usf-service.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends BaseRouter implements OnInit {
    username: string;
    dealerCode: string;
    roleName: string;
    directedSell: boolean;
    version = '';

    constructor(
        public authenticationService: AuthenticationService, public router: Router, private usfServiceService: UsfServiceService) {
        super(router);
        if (authenticationService.isAuthenticated()) {
            this.username = authenticationService.getCredentials().UserName;
            this.dealerCode = authenticationService.getCredentials().DealerCode;
            this.roleName = authenticationService.getCredentials().RoleName;
            this.directedSell = this.isDirectedSell();
        }
    }

    ngOnInit() {
        this.usfServiceService.getAppVersion().then(version => {
            this.version = version;
        });
    }

    validateSession() {
        if (this.authenticationService !== undefined) {
            return this.authenticationService.getCredentials() !== null;
        } else {
            return false;
        }
    }

    isDirectedSell() {
        return (
            this.roleName.toUpperCase() === 'USF-SUP' ||
            this.roleName.toUpperCase() === 'USF-AGENT' ||
            this.roleName.toUpperCase() === 'USF-REPSTORE'
        );
    }
}
