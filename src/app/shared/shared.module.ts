import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from 'src/app/shared/components/header/header.component';
import { NgbRangeDatepicker } from './components/range-datepicker/range-datepicker';
import { LoadingModal } from './components/loading-modal/loading.modal';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { LoadingService } from './components/loading-modal/loading.service';

@NgModule({
    declarations: [
        HeaderComponent,
        NgbRangeDatepicker,
        LoadingModal
    ],
    imports: [
        CommonModule,
        NgbModule,
        RouterModule
    ],
    exports: [
        HeaderComponent,
        NgbRangeDatepicker,
        LoadingModal
    ],
    providers: [
        LoadingService
    ]
})
export class SharedModule {
}
