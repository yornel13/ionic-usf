import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {UniversalServiceComponent} from './universal-service.component';
import {PersonalDatesComponent} from 'src/app/universal-service/components/personal-dates/personal-dates.component';
// tslint:disable-next-line:max-line-length
import {SocialSecureVerificationComponent} from 'src/app/universal-service/components/social-secure-verification/social-secure-verification.component';
import {AddressDateComponent} from 'src/app/universal-service/components/address-date/address-date.component';
import {RegisterCaseComponent} from 'src/app/universal-service/components/register-case/register-case.component';
import {UsfVerificationComponent} from 'src/app/universal-service/components/usf-verification/usf-verification.component';
// tslint:disable-next-line:max-line-length
import {DocumentDigitalizationComponent} from 'src/app/universal-service/components/document-digitalization/document-digitalization.component';
import {AccountCreationComponent} from 'src/app/universal-service/components/account-creation/account-creation.component';
import {AceptationTermsComponent} from 'src/app/universal-service/components/aceptation-terms/aceptation-terms.component';
import {ActivationComponent} from 'src/app/universal-service/components/activation/activation.component';

import {SignaturePadModule} from 'angular2-signaturepad';

const routes: Routes = [
    {
        path: 'universal-service',
        component: UniversalServiceComponent,
        children: [
            {path: 'personal-dates', component: PersonalDatesComponent},
            {path: 'social-secure-verification', component: SocialSecureVerificationComponent},
            {path: 'address-date', component: AddressDateComponent},
            {path: 'register-case', component: RegisterCaseComponent},
            {path: 'usf-verification', component: UsfVerificationComponent},
            {path: 'document-digitalization', component: DocumentDigitalizationComponent},
            {path: 'account-creation', component: AccountCreationComponent},
            {path: 'aceptation-terms', component: AceptationTermsComponent},
            {path: 'activation', component: ActivationComponent}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule, SignaturePadModule],
    providers: []
})
export class UniversalServiceRoutingModule {
}
