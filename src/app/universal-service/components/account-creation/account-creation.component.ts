import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '../../../core/base/BaseComponent';
import { UsfServiceService } from '../../../core/usf/usf-service.service';
import { AuthenticationService } from '../../../core';
import { LoadingService } from '../../../shared/components/loading-modal/loading.service';

declare let alertify: any;

export interface Model {
    accountType: string;
    tecnology: string;
    planType: string;
    imei: string;
    simCard: string;
}

export interface Plan {
    plan: string;
    planName: string;
    planDetails: string[];
}

@Component({
    selector: 'app-account-creation',
    templateUrl: './account-creation.component.html',
    styleUrls: ['./account-creation.component.scss']
})
export class AccountCreationComponent extends BaseComponent implements OnInit {

    processValidationSIF = false;

    public accountTypes = ['Prepago Móvil'];
    public planTypes = ['Móvil Prepago'];
    public tecnologies = ['GSM'];
    public locations: any[];
    public thereIsLocations = false;
    public selectedLocation = '';
    public step = 'step1';
    public form: FormGroup;
    loading = false;
    model: Model = new (class implements Model {
        accountType = '';
        tecnology = '';
        planType = '';
        imei = '';
        simCard = '';
    })();
    /*
    {
      plan: 'Plan 2099',
      planDetails: [
        '1,000 minutos para uso de voz local, larga distancia a Estados Unidos' +
          ' y “Roaming” en EEUU compartido. Costo del minuto adicional es 10¢.',
        '400 SMS/MMS locales, a EEUU y a ciertos destinos internacionales',
        'Costo SMS/MMS adicional enviado es de 10¢.',
        'SMS/MMS recibidos son gratis.'
      ]
    },
    */
    plans: Plan[] = [
        {
            plan: 'Plan 2219',
            planName: '2219',
            planDetails: [
                '1,100 minutos para uso de voz local, larga distancia a Estados Unidos' + ' y “Roaming” en EEUU.',
                '1,000 SMS/MMS en PR & USA.',
                'SMS/MMS recibidos son gratis.',
                ' 3 GB de data para uso en PR y en EEUU con Bloqueo.'
            ]
        },
        {
            plan: 'Plan 2219 LTE',
            planName: '2219LTE',
            planDetails: [
                '1,100 minutos para uso de voz local, larga distancia a Estados Unidos' + ' y “Roaming” en EEUU.',
                '1,000 SMS/MMS en PR & USA.',
                'SMS/MMS recibidos son gratis.',
                ' 3 GB de data para uso en PR y en EEUU con Bloqueo.'
            ]
        }
    ];

    planSelected: Plan = undefined;

    public checkImeiValidated = false;

    public dealer: any;
    checkLDI: any = null;

    processValidation = false;
    toBackEnd = false;
    msjError = '';
    msjErrorDisplay = '';
    caseIDReject: any = null;
    suscriberNumber: any = null;
    banNumber: any = null;

    currentDate: any = {
        dd: new Date().getDate() < 10 ? '0' + new Date().getDate() : new Date().getDate(),
        mm: new Date().getMonth() < 9 ? '0' + (new Date().getMonth() + 1) : new Date().getMonth() + 1,
        yy: new Date().getFullYear()
    };
    stepOneDone = false;

    constructor(
        private loadingService: LoadingService,
        public authenticationService: AuthenticationService,
        public usfServiceService: UsfServiceService,
        public router: Router,
        public fb: FormBuilder
    ) {
        super(authenticationService, usfServiceService, router, fb);

        if (this.dataObjectAddress[0] !== undefined && this.dataObjectAddress[0].CUSTOMER_ADDRESS !== undefined) {
            this.dataObjectAddress[0].CUSTOMER_ADDRESS = this.convertAccents(this.dataObjectAddress[0].CUSTOMER_ADDRESS);
        }
    }

    ngOnInit() {
        window.scroll(0, 0);

        this.dealer = this.authenticationService.credentials.Dealer;

        this.dealer.toString() === '0' ? this.getLocationsCustom() : this.getLocations();

        this.form = this.fb.group({
            accountType: [null, Validators.compose([Validators.required])],
            tecnology: [null, Validators.compose([Validators.required])],
            planType: [null, Validators.compose([Validators.required])],
            selectedLocation: [null, Validators.compose([Validators.required])],
            imei: [null, Validators.compose([Validators.required])],
            simCard: [null, Validators.compose([Validators.required])]
        });
    }

    goToAceptationTerms() {
        if (
            this.form.valid &&
            this.checkImeiValidated &&
            this.model.imei.length === 15 &&
            this.model.simCard.length === 20 &&
            this.selectedLocation !== undefined &&
            this.selectedLocation !== ''
        ) {
            this.stepOneDone = false;
            this.loadingService.startLoading('Inicia proceso de creación de cuenta, espere por favor...');
            this.processValidationSIF = true;
            const datos = {
                method: 'CreateNewAccountMcapi',
                UserID: this.authenticationService.credentials.userid,
                caseID: this.validateSSNData.CASE_NUMBER,
                mAccountType: 'I',
                mAccountSubType: 'P',
                customer_ssn: this.usfServiceService.getValue('ssn').toString().replace('-', ''),
                SIMSerial: this.model.simCard,
                IMEISerial: this.model.imei,
                tech: this.model.tecnology,
                mSocCode: this.planSelected.planName,
                USF_LOCATION: this.selectedLocation
            };

            this.loading = true;
            this.usfServiceService.doAction(datos).subscribe(resp => {
                this.loading = false;
                if (!resp.HasError) {
                    this.stepOneDone = true;
                    // para ser usado en caso de excepcion en ultima ventana
                    localStorage.setItem('simCard', this.model.simCard);
                    // this.router.navigate(['/universal-service/aceptation-terms'], { replaceUrl: true });
                    const datos3 = {
                        method: 'CreateSubscriberMcapi',
                        UserID: this.authenticationService.credentials.userid,
                        caseID: this.validateSSNData.CASE_NUMBER,
                        DealerFl: this.dealer
                    };

                    this.usfServiceService.doAction(datos3).subscribe(
                        resp3 => {
                            this.loadingService.stopLoading();
                            this.processValidationSIF = false;
                            this.processValidation = false;
                            this.suscriberNumber = resp3.subscriber;
                            this.banNumber = resp3.mBan;
                            if (!resp3.HasError) {
                                sessionStorage.setItem('suscriberNumber', resp3.subscriber);
                                this.router.navigate(['/universal-service/activation'], { replaceUrl: true });
                            } else {
                                this.stepOneDone = false;
                                if (resp3.ErrorDesc.toLocaleLowerCase().indexOf('enviado al back end')) {
                                    this.suscriberNumber = localStorage.getItem('phone1');
                                    // ottro numero es localStorage.getItem('simCard')
                                    this.msjError = resp3.ErrorDesc;
                                    this.msjErrorDisplay = resp3.Errordisplay;
                                    this.toBackEnd = true;
                                    this.caseIDReject = datos3.caseID;
                                } else {
                                    alertify.alert('Aviso', resp3.ErrorDesc, () => {
                                        this.goTo('/home');
                                    });
                                }
                            }
                        },
                        error => {
                            this.processValidation = false;
                        }
                    );
                } else {
                    this.loadingService.stopLoading();
                    this.processValidationSIF = false;
                    this.processValidation = false;

                    localStorage.setItem('simCard', null);

                    alertify.alert(
                        'Aviso',
                        // tslint:disable-next-line:max-line-length
                        'Ha ocurrido un error intentando crear la cuenta, recuerde que al segundo intento el caso será enviado al Back End.',
                        () => {
                            this.manageStep();
                        }
                    );
                }
            });
        }
    }

    manageStep() {
        if (this.step === 'step1') {
            this.step = 'step2';
        } else {
            // this.goToStep2();
        }
    }

    goToDocumentDigitalization() {
        this.router.navigate(['/universal-service/document-digitalization'], { replaceUrl: true });
    }

    onCheckChange($event: any) {
        this.checkImeiValidated = !this.checkImeiValidated;
    }

    setPlan($event: any) {
        this.planSelected = this.plans.find(x => x.plan === $event);
    }

    getLocations() {
        const data = this.usfServiceService.getValue('credentials');
        let loadLocations = [];
        this.locations = [];
        loadLocations = data.Locations;

        if (loadLocations.length > 0) {
            this.thereIsLocations = true;
            for (const loadLocation of loadLocations) {
                this.locations.push(loadLocation);
            }
        }
    }

    getLocationsCustom() {
        this.locations = [];

        const datos = {
            method: 'getLocationMcapi'
        };

        this.usfServiceService.doAction(datos).subscribe(resp => {

            if (resp.length > 0) {
                this.thereIsLocations = true;

                // @ts-ignore
                resp.forEach(item => {
                    this.locations.push(item.location_sif);
                });
            }
        });
    }

    getSuggestAddress() {
        return this.dataObjectAddress.length === 3 ?
            this.dataObjectAddress[2].SUGGEST_ADDRESS : this.dataObjectAddress[0].SUGGEST_ADDRESS;
    }
}
