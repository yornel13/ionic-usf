import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {BaseComponent} from '../../../core/base/BaseComponent';
import {AuthenticationService} from '../../../core';
import {UsfServiceService} from '../../../core/usf/usf-service.service';

declare let alertify: any;

export interface Model {
    CUSTOMER_NAME: string;
    CUSTOMER_LAST: string;
    USF_CASE_ID: string;
    mBan: string;
    subscriber: string;
}

@Component({
    selector: 'app-activation',
    templateUrl: './activation.component.html',
    styleUrls: ['./activation.component.scss']
})
export class ActivationComponent extends BaseComponent implements OnInit {
    model: Model = new (class implements Model {
        CUSTOMER_NAME = '';
        CUSTOMER_LAST = '';
        USF_CASE_ID = '';
        mBan = '';
        subscriber = '';
    })();

    public subscriberNumber: string;
    public dealer: any;
    public message: string;

    constructor(
        public authenticationService: AuthenticationService,
        public usfServiceService: UsfServiceService,
        public router: Router,
        public fb: FormBuilder
    ) {
        super(authenticationService, usfServiceService, router, fb);

        const userId = this.authenticationService.credentials.userid;
        const caseId = this.validateSSNData.CASE_NUMBER;

        // this.subscriberActivation = true;
        this.dealer = this.authenticationService.credentials.Dealer;

        if (this.dealer.toString() === '0') {
            this.message = 'Su Número de Telefono prepago está en proceso de activacion';
        } else {
            this.message = 'Su Número de Telefono prepago  ya está activo y listo para su uso';
        }

        const data = {
            method: 'getBanMcapi',
            UserID: userId,
            caseID: caseId
        };

        this.usfServiceService.doAction(data).subscribe(
            resp => {

                // this.subscriberActivation = false;

                if (!resp.HasError) {
                    this.model = resp;

                    this.subscriberNumber = sessionStorage.getItem('suscriberNumber');
                } else {
                    alertify.alert('Aviso', resp.ErrorDesc, () => {
                        this.goTo('/home');
                    });
                }
            },
            error => {
            }
        );
    }

    ngOnInit() {
        window.scroll(0, 0);
    }
}
