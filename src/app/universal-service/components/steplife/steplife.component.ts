import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
    selector: 'app-steplife',
    templateUrl: './steplife.component.html',
    styleUrls: ['./steplife.component.scss']
})
export class SteplifeComponent implements OnInit {

    path: string;

    constructor(private router: Router) {
        router.events.subscribe(value => {
            if (value instanceof NavigationEnd) {
                this.path = value.url;
            }
        });
    }

    ngOnInit() {
    }

}
