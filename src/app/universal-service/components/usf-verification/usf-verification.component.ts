import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbCalendar, NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { BaseComponent } from '../../../core/base/BaseComponent';
import { AuthenticationService } from '../../../core';
import { UsfServiceService } from '../../../core/usf/usf-service.service';
import InputValidationUtil from '../../../util/input.validation.util';

export interface Model {
    firstName: string;
    secondName: string;
    lastName: string;
    birthday: string;
    suffix: string;
    socialSecure: string;
}

@Component({
    selector: 'app-usf-verification',
    templateUrl: './usf-verification.component.html',
    styleUrls: ['./usf-verification.component.scss']
})
export class UsfVerificationComponent extends BaseComponent implements OnInit {

    datePickerIsInit = false;
    processValidationNLAD = false;

    valueSSN = '';
    checkSSN = false;
    loading = false;

    birthday: NgbDate;
    birthdayMinDate: NgbDate;
    birthdayMaxDate: NgbDate;

    public suffixes = ['MR', 'MRS', 'ENG', 'ATTY', 'DR'];

    public form: FormGroup;

    model: Model = new (class implements Model {
        suffix = '';
        socialSecure = '';
        birthday = '';
        firstName = '';
        lastName = '';
        secondName = '';
    }) ();

    constructor(
        private calendar: NgbCalendar,
        public authenticationService: AuthenticationService,
        public usfServiceService: UsfServiceService,
        public router: Router,
        public fb: FormBuilder
    ) {
        super(authenticationService, usfServiceService, router, fb);
        this.birthdayMinDate = calendar.getPrev(calendar.getToday(), 'y', 100);
        this.birthdayMaxDate = calendar.getPrev(calendar.getToday(), 'y', 21);
    }

    ngOnInit() {
        window.scroll(0, 0);

        this.form = this.fb.group({
            suffix: [null, Validators.compose([Validators.required])],
            firstName: [null, Validators.compose([Validators.required])],
            secondName: [
                null,
                Validators.compose([
                    // Validators.required
                ])
            ],
            lastName: [null, Validators.compose([Validators.required])],
            socialSecure: [null, Validators.compose([Validators.required])],
            birthday: [null, Validators.compose([Validators.required])]
        });
    }

    goToDocumentDigitization() {
        if (this.form.valid && this.model.socialSecure.length === 11) {
            this.processValidationNLAD = true;

            const data = {
                method: 'CreateDepend',
                CASE_ID: this.validateSSNData.CASE_NUMBER,
                DEPENDENT_SUFFIX: this.model.suffix,
                DEPENDENT_NAME: this.model.firstName,
                DEPENDENT_MN: this.model.secondName,
                DEPENDENT_LAST: this.model.lastName,
                DEPENDENT_DOB: this.moment(this.model.birthday).format('YYYY-MM-DD'),
                DEPENDENT_SSN: String(this.valueSSN)
                    .replace('-', '')
                    .replace('-', '')
                    .replace('-', '')
            };

            this.loading = true;
            setTimeout(() => {
                this.usfServiceService.doAction(data).subscribe(resp => {
                    this.loading = false;
                    this.processValidationNLAD = false;
                    this.usfServiceService.setValue('requiredDocuments', resp.required);
                    if (!resp.HasError) {
                        this.goTo('/universal-service/address-date');
                    } else {
                        this.alertify.alert('Aviso', resp.ErrorDesc);
                    }
                });
            }, 2000);
        }
    }

    back() {
        this.router.navigate(['/universal-service/personal-dates'], { replaceUrl: true });
    }

    // tslint:disable-next-line:member-ordering
    public formatDate(entry: string) {
        // NO se aceptara mas de 10 caracteres
        if (entry.length > 10) {
            entry = entry.substr(0, 10);
        }
        // Limpiando especificando caracteres no permitidos
        const patron = /abcdefghijklmnopqrstuvwxyz/gi;
        const nuevoValor = '';
        entry = entry.replace(patron, nuevoValor);

        if (entry.length > 2 && entry.indexOf('/') !== 2) {
            entry = entry.replace('/', '');
            entry = entry.substr(0, 2) + '/' + entry.substr(2, entry.length);
        }

        if (entry.length > 5 && entry.indexOf('/', 5) !== 5) {
            // caso para el 2do Slash
            entry = entry.substr(0, 5) + '/' + entry.substr(5, 4);
        }
        if (entry.length >= 10) {

        }
        return entry;
    }

    onBlurSSN() {
        if (this.model.socialSecure !== undefined) {
            // tslint:disable-next-line: radix
            if (
                this.model.socialSecure.length === 11 &&
                // tslint:disable-next-line: radix
                (parseInt(
                    String(this.valueSSN)
                        .replace('-', '')
                        .replace('-', '')
                    ) >= 99999999 ||
                    // tslint:disable-next-line: radix
                    parseInt(
                        String(this.valueSSN)
                            .replace('-', '')
                            .replace('-', '')
                    ) >= 9999999)
            ) {
                // this.model.socialSecure = 'XXX-XX-' + this.valueSSN.substr(5, 4);

                // si tiene los 2 - guiones  ya esta validado
                let remplazo = String(this.valueSSN.replace('-', '').replace('-', '')).replace('-', '');
                remplazo = 'XXX' + '-' + 'XX' + '-' + String(this.valueSSN).substr(-4, 4);

                this.model.socialSecure = remplazo;
            } else {
                this.model.socialSecure = undefined;
                this.valueSSN = undefined;
                this.checkSSN = false;
            }
        }
    }

    onFocusSSN() {
        if (this.model.socialSecure !== undefined) {
            // tslint:disable-next-line: radix
            if (
                this.model.socialSecure.length === 11 &&
                // tslint:disable-next-line: radix
                (parseInt(
                    String(this.valueSSN)
                        .replace('-', '')
                        .replace('-', '')
                    ) >= 99999999 ||
                    // tslint:disable-next-line: radix
                    parseInt(
                        String(this.valueSSN)
                            .replace('-', '')
                            .replace('-', '')
                    ) >= 9999999)
            ) {
                // si trae guiones se limpia
                this.valueSSN = String(this.valueSSN)
                    .replace('-', '')
                    .replace('-', '')
                    .replace('-', '');

                // Restaurando el valor
                this.model.socialSecure =
                    String(this.valueSSN).substr(0, 3) +
                    '-' +
                    String(this.valueSSN).substr(3, 2) +
                    '-' +
                    String(this.valueSSN).substr(5, 4);
            } else {
                this.model.socialSecure = undefined;
                this.valueSSN = undefined;
                this.checkSSN = false;
            }
        }
    }

    formatInputSocialSecure(input: string) {
        const result = InputValidationUtil.setFormatInputCase(
            this.form.get('socialSecure').value,
            'ssn',
            this.valueSSN,
            InputValidationUtil.validateRightSSN(this.form.get('socialSecure').value, this.valueSSN)
        );
        this.form.get('socialSecure').setValue(result.valueSSN);
        this.checkSSN = result.checkSSN;
        this.valueSSN = result.valueSSN;
    }

    async closeBirthdayPicker() {
        await this.delay(250);
        if (!this.birthday || this.birthday.year > new Date(new Date().setFullYear(new Date().getFullYear() - 21)).getFullYear()) {
            this.model.birthday = '';
        } else {
            this.model.birthday = this.toModel(this.birthday);
        }
    }

    toModel(date: NgbDate | null): string | null {
        return date ? date.year + '-' + (date.month < 10 ? '0' : '' ) + date.month + '-' + (date.day < 10 ? '0' : '' ) + date.day : null;
    }
}
