import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseComponent } from '../../../core/base/BaseComponent';
import { AuthenticationService } from '../../../core';
import { LoadingService } from '../../../shared/components/loading-modal/loading.service';
import { UsfServiceService } from '../../../core/usf/usf-service.service';

declare let alertify: any;

@Component({
    selector: 'app-register-case',
    templateUrl: './register-case.component.html',
    styleUrls: ['./register-case.component.scss']
})
export class RegisterCaseComponent extends BaseComponent implements OnInit, AfterViewInit {

    @ViewChild('modalProcessValidationApplicationId', { static: false }) modalProcessValidationApplicationId: any;
    @ViewChild('modalProcessValidationNLAD', { static: false }) modalProcessValidationNLAD: any;

    subscriberVerificationObject: any = {};
    loading = false;
    removedCase = false;

    constructor(
        private loadingService: LoadingService,
        private modalService: NgbModal,
        public authenticationService: AuthenticationService,
        public router: Router,
        public usfServiceService: UsfServiceService,
        public fb: FormBuilder
    ) {
        super(authenticationService, usfServiceService, router, fb);
        // Formateando Acentos
        if (this.dataObjectAddress[0] !== undefined && this.dataObjectAddress[0].CUSTOMER_ADDRESS !== undefined) {
            this.dataObjectAddress[0].CUSTOMER_ADDRESS = this.convertAccents(this.dataObjectAddress[0].CUSTOMER_ADDRESS);
        }

        this.subscriberVerificationObject = this.usfServiceService.getValue('subscriberVerificationObject');

    }

    ngAfterViewInit() {
        if (this.usfServiceService.getValue('applicationId') !== null) {
            if (this.subscriberVerificationObject.applicationId !== this.usfServiceService.getValue('applicationId')) {
                this.openModal(this.modalProcessValidationApplicationId);
            }
        }
        if (this.subscriberVerificationObject.HasError) {
            this.deleteCase();
        }
    }

    deleteCase() {
        const datos3 = {
            method: 'DeleteCaseAllMcapi',
            USER_ID: this.authenticationService.credentials.userid,
            CASE_ID: this.validateSSNData.CASE_NUMBER
        };
        this.loadingService.startLoading('Su caso esta siendo eliminado, por favor espere...');
        this.usfServiceService.doAction(datos3).subscribe(
            () => {
                this.removedCase = true;
                this.loadingService.stopLoading();
            }, () => {
                setTimeout(() => {
                    this.deleteCase();
                }, 5000);
            }
        );
    }

    openModal(content: any) {
        this.modalService.open(
            content,
            {
                ariaLabelledBy: 'modal-basic-title',
                centered: true, size: 'lg', backdrop: 'static'
            }).result.then();
    }

    ngOnInit() {
        window.scroll(0, 0);

        this.form = this.fb.group({
            eligibilityCheckId: ['', Validators.compose([Validators.required])],
            idNumber: ['', Validators.compose([])],
            elegibilityCheck: [null, Validators.compose([Validators.required])]
        });
    }

    goToUsfVerification() {
        if (this.subscriberVerificationObject.status === 'COMPLETE' &&
            this.subscriberVerificationObject.activeSubscriber === 'N' &&
            !this.subscriberVerificationObject.HasError) {
            this.router.navigate(['/universal-service/document-digitalization'], { replaceUrl: true });
        }
    }

    getSnn(SSN: string) {
        return SSN.length === 3 ? '0' + SSN : SSN;
    }

    goHome() {
        if (this.removedCase) {
            this.router.navigate(['/home'], { replaceUrl: true });
        } else {
            this.openModal(this.modalProcessValidationNLAD);
        }
    }
}
