import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { NgbCalendar, NgbDate, NgbDateParserFormatter, NgbInputDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { BaseComponent } from '../../../core/base/BaseComponent';
import { AuthenticationService } from '../../../core';
import { UsfServiceService } from '../../../core/usf/usf-service.service';

export interface Model {
    birthday: string;
    idExpirationDate: string;
}

@Component({
    selector: 'app-personal-dates',
    templateUrl: './personal-dates.component.html',
    styleUrls: ['./personal-dates.component.scss'],
    providers: [NgbInputDatepickerConfig]
})
export class PersonalDatesComponent extends BaseComponent implements OnInit, AfterViewInit {

    checkSSN = false;
    valueSSN = '';
    processValidationSIF = false;
    valueBirthday = '';
    valueExpirationDate = '';
    inputControl: any;
    inputControl2: any;
    subscriberVerificationObject: any = null;
    dealer: number;
    @ViewChild('birthdayDemo') birthdayDemoS: ElementRef;
    back = false;

    public suffixes = ['MR.', 'MRS.', 'ENG.', 'ATTY.', 'DR.'];
    public idTypes = ['Licencia de Conducir', 'Pasaporte'];
    public dependPeopleFlag: FormControl = new FormControl(false, Validators.minLength(2));
    model: Model = new (class implements Model {
        birthday = '';
        idExpirationDate = '';
    })();

    dataAgency: any[] = null;
    assistance = true;
    repID = '';
    suffix = '';
    firstName = '';
    secondName = '';
    lastName = '';
    socialSecure = '';
    gender: boolean = undefined;
    idType = '';
    idNumber = '';
    dependence = false;
    public strListAgent: any[] = [];
    public checkListAgency: any[] = [];
    loading = false;
    birthdayMinDate: NgbDate;
    birthdayMaxDate: NgbDate;
    expirationMinDate: NgbDate;
    expirationMaxDate: NgbDate;

    constructor(
        private formatter: NgbDateParserFormatter,
        private calendar: NgbCalendar,
        public authenticationService: AuthenticationService,
        public usfServiceService: UsfServiceService,
        public router: Router,
        public fb: FormBuilder
    ) {
        super(authenticationService, usfServiceService, router, fb);
        this.dealer = this.authenticationService.credentials.Dealer;
        this.subscriberVerificationObject = this.usfServiceService.getValue('subscriberVerificationObject');
        this.birthdayMinDate = calendar.getPrev(calendar.getToday(), 'y', 100);
        this.birthdayMaxDate = calendar.getPrev(calendar.getToday(), 'y', 21);
        this.expirationMinDate = calendar.getToday();
        this.expirationMaxDate = calendar.getNext(calendar.getToday(), 'y', 12);
    }

    async ngOnInit() {
        window.scroll(0, 0);
        const data: any = JSON.parse(localStorage.getItem('personalData'));

        this.dataAgency = this.usfServiceService.getValue('agencies');

        if (data !== null) {
            const agencyList = data.strListAgent.split(',');
            for (const object of this.dataAgency) {
                let checked = false;
                for (const agency of agencyList) {
                    if (agency === object.cod_id) {
                        checked = true;
                    }
                }
                const a = Object.defineProperty(object, 'checked', {
                    value: checked,
                    configurable: true
                });
                this.checkListAgency.push(a);
                if (checked) {
                    this.strListAgent.push(object.cod_id);
                }
            }

            this.assistance = data.asistence;
            this.repID = data.repId;
            this.suffix = data.CUSTOMER_SUFFIX;
            this.firstName = data.CUSTOMER_NAME;
            this.secondName = data.CUSTOMER_MN;
            this.lastName = data.CUSTOMER_LAST;
            this.socialSecure = 'XXX-XX-' + String(data.CUSTOMER_SSN).substr(-4, 4);
            this.valueSSN = data.CUSTOMER_SSN;
            this.checkSSN = true;

            this.gender = data.GENDER === 1;
            this.idType = data.CUSTOMER_ID_TYPE === 0 ? this.idTypes[1] : this.idTypes[0];
            this.idNumber = data.ID_NUMBER;

            this.inputControl = NgbDate.from(this.formatter.parse(data.CUSTOMER_DOB));
            this.valueBirthday = data.CUSTOMER_DOB;
            this.model.birthday = data.CUSTOMER_DOB;

            this.inputControl2 = NgbDate.from(this.formatter.parse(data.DTS_EXP));
            this.model.idExpirationDate = data.DTS_EXP;
            this.valueExpirationDate = data.DTS_EXP;

            this.dependence = data.dependence;
        } else {
            for (const object of this.dataAgency) {
                const a = Object.defineProperty(object, 'checked', {
                    value: false,
                    configurable: true
                });
                this.checkListAgency.push(a);
            }
        }
        this.form = this.fb.group({
            suffix: ['', Validators.compose([Validators.required])],
            firstName: ['', Validators.compose([Validators.required])],
            secondName: ['', Validators.compose([])],
            lastName: ['', Validators.compose([Validators.required])],
            socialSecure: ['', Validators.compose([Validators.required])],
            birthday: [null, Validators.compose([Validators.required])],
            gender: [null, Validators.compose([Validators.required])],
            idType: ['', Validators.compose([Validators.required])],
            idNumber: ['', Validators.compose([Validators.required])],
            idExpirationDate: [null, Validators.compose([Validators.required])],
            liveWithAnotherAdult: [null, Validators.compose([])],
            hasLifelineTheAdult: [null, Validators.compose([])],
            sharedMoneyWithTheAdult: [null, Validators.compose([])],
            assistance: [null, Validators.compose([Validators.required])]
        });
    }

    goToSocialSecureVerification() {
        if (
            this.validateForm()
        // true
        ) {
            const datos: any = {
                method: 'validareSSNAdMcapi',
                CUSTOMER_SUFFIX: this.form.get('suffix').value,
                USER_ID: this.authenticationService.credentials.userid.toString(),
                CUSTOMER_NAME: this.form.get('firstName').value,
                CUSTOMER_MN: this.form.get('secondName').value,
                CUSTOMER_LAST: this.form.get('lastName').value,
                CUSTOMER_SSN: this.valueSSN
                    .replace('-', '')
                    .replace('-', '')
                    .replace('-', ''),
                CUSTOMER_DOB: this.valueBirthday,
                GENDER: this.form.get('gender').value ? '1' : '0',
                CUSTOMER_ID_TYPE: this.form.get('idType').value === 'Pasaporte' ? '0' : '1',
                ID_NUMBER: this.form.get('idNumber').value,
                DTS_EXP: this.valueExpirationDate,
                DealerFl: this.dealer,
                strListAgent: this.strListAgent.toString(),
                strEliAsited: this.form.get('assistance').value ? '1' : '0',
                repId: this.repID,
                asistence: this.assistance
            };

            localStorage.setItem('valueSSN', this.valueSSN);
            // localStorage.setItem('SSN', );
            // this.form.get('birthday').setValue('02/02/2019');
            this.loading = true;
            this.usfServiceService.doAction(datos).subscribe(resp => {
                this.loading = false;
                this.processValidationSIF = false;
                this.usfServiceService.setValue('validateSSNData', resp);

                if (!resp.HasError) {
                    // Limpiando en caso satisfactorio
                    localStorage.setItem('existSSNCase', null);
                    localStorage.setItem('existSSNCaseName', null);
                    localStorage.setItem('existSSNCaseNumber', null);
                    localStorage.setItem('existSSNCaseSSN', null);
                    localStorage.setItem('existSSNCasePhone', null);
                    localStorage.setItem('existSSNCaseAddress', null);
                    localStorage.setItem('existSSNCaseBan', null);
                    localStorage.setItem('lifelineActivationDate', null);
                    localStorage.setItem('accountType', null);

                    datos.dependence = this.dependence;
                    localStorage.setItem('personalData', JSON.stringify(datos));

                    this.usfServiceService.setValue('ssn', this.valueSSN.replace('-', '').replace('-', ''));

                    if (this.usfServiceService.getValue('validateSSNData').CASENUMBER !== 0) {
                        if (this.dependPeopleFlag.value) {
                            this.router.navigate(['/universal-service/usf-verification'], { replaceUrl: true }).then();
                        } else {
                            localStorage.setItem('personalData', null);
                            localStorage.setItem('valueSSN', null);
                            this.router.navigate(['/universal-service/address-date'], { replaceUrl: true }).then();
                        }
                    } else {
                        this.alertify.alert(
                            'Aviso',
                            'Hemos detectado un error en la creación del caso. Su caso fue eliminado.',
                            () => {
                                this.goTo('/home');
                            }
                        );
                    }
                } else {
                    if (resp.dataObject.length > 0) {
                        // cuando viene en dataObject
                        // es que NO se Registro completamente
                        localStorage.setItem('existSSNCase', 'incomplete');
                        localStorage.setItem('existSSNCaseName', resp.dataObject[0].name);
                        localStorage.setItem('existSSNCaseNumber', resp.dataObject[0].CASENUMBER);
                        localStorage.setItem('existSSNCaseSSN', resp.dataObject[0].ssn);
                        localStorage.setItem('existSSNCasePhone', resp.dataObject[0].phone1);
                        localStorage.setItem('existSSNCaseAddress', resp.dataObject[0].address);
                        localStorage.setItem('existSSNCaseBan', resp.dataObject[0].ban);
                        localStorage.setItem('accountType', resp.dataObject[0].accountType);
                        localStorage.setItem('lifelineActivationDate', resp.dataObject[0].efectivedate);
                    } else {
                        // en data es que SI se Registro completamente
                        localStorage.setItem('existSSNCase', resp.data[0]);
                        localStorage.setItem('existSSNCaseName', resp.data[0].name);
                        localStorage.setItem('existSSNCaseNumber', resp.data[0].CASENUMBER);
                        localStorage.setItem('existSSNCaseSSN', resp.data[0].ssn);
                        localStorage.setItem('existSSNCasePhone', resp.data[0].subscriberNumber);
                        localStorage.setItem('existSSNCaseAddress', resp.data[0].address);
                        localStorage.setItem('existSSNCaseBan', resp.data[0].ban);
                        localStorage.setItem('lifelineActivationDate', resp.data[0].lifelineActivationDate);
                        localStorage.setItem('accountType', resp.data[0].accountType);
                    }
                    this.router.navigate(['/universal-service/social-secure-verification'], { replaceUrl: true });
                }
            });
        }
    }

    public setFormatInputSSN() {
        this.form.get('socialSecure').setValue(this.formatInput());
    }

    public formatInput() {
        const input: string = this.form.get('socialSecure').value;
        if (this.validateRightSSN()) {
            this.valueSSN = input.replace(/-/g, '');
            this.valueSSN =
                String(this.valueSSN).substr(0, 3) +
                '-' +
                String(this.valueSSN).substr(3, 2) +
                '-' +
                String(this.valueSSN).substr(5, 4);
            this.checkSSN = true;
            return this.valueSSN;
        }

        if (input.length > 1 && String(input).substr(input.length - 1, 1) !== 'X') {
            this.valueSSN += String(input.substr(input.length - 1, 1));
        } else if (input !== 'X' && input !== 'XXX-XX-XXXX') {
            this.valueSSN = input;
        }

        if (input.length === 4 || input.length === 7) {
            return (
                input.substr(0, input.length - 1) +
                (input[input.length - 1] === '-' ? '' : '-') +
                input.substr(input.length - 1, input.length)
            );
        } else {
            return input;
        }
    }

    onBlurSSN() {
        if (this.validateRightSSN()) {
            this.form.get('socialSecure').setValue('XXX-XX-' + String(this.valueSSN).substr(-4, 4));
        } else {
            this.form.get('socialSecure').setValue('');
            this.valueSSN = undefined;
            this.checkSSN = false;
        }
    }

    onFocusSSN() {
        if (this.validateRightSSN()) {
            this.valueSSN = String(this.valueSSN).replace(/-/g, '');

            this.form
                .get('socialSecure')
                .setValue(
                    String(this.valueSSN).substr(0, 3) +
                    '-' +
                    String(this.valueSSN).substr(3, 2) +
                    '-' +
                    String(this.valueSSN).substr(5, 4)
                );
        } else {
            this.form.get('socialSecure').setValue('');
            this.valueSSN = undefined;
            this.checkSSN = false;
        }
    }

    validateRightSSN() {
        return (
            this.form.get('socialSecure').value.length === 11 &&
            parseInt(String(this.valueSSN).replace(/-/g, ''), 10) >= 9999999
        );
    }

    validateForm() {
        let valid;
        if (this.back === false) {
            if (this.assistance) {
                this.repID.length > 0 ? valid = true : valid = false;
            } else {
                valid = true;
            }
        } else {
            valid = true;
        }
        return (this.form.valid && valid);
    }

    setCheck(param: any) {
        if (this.strListAgent.length > 0) {
            let find = 0;
            for (let i = 0; i < this.strListAgent.length; i++) {
                if (this.strListAgent[i] === param) {
                    this.strListAgent.splice(i, 1);
                    find = 1;
                    break;
                }
            }
            if (find === 0) {
                this.strListAgent.push(param);
            }
        } else {
            this.strListAgent.push(param);
        }
    }

    ngAfterViewInit() {

    }

    async closeBirthdayPicker() {
        await this.delay(250);

        if (!this.inputControl || this.inputControl.year > new Date(new Date().setFullYear(new Date().getFullYear() - 21)).getFullYear()) {
            this.valueBirthday = '';
            this.model.birthday = '';
            this.inputControl = undefined;
        } else {
            this.valueBirthday = this.toModel(this.inputControl);
            this.model.birthday = this.toModel(this.inputControl);
        }
    }

    async closeExpirationPicker() {
        await this.delay(250);
        if (this.inputControl2) {
            this.model.idExpirationDate = this.toModel(this.inputControl2);
            this.valueExpirationDate = this.toModel(this.inputControl2);
        }
    }

    toModel(date: NgbDate | null): string | null {
        return date ? date.year + '-' + (date.month < 10 ? '0' : '') + date.month + '-' + (date.day < 10 ? '0' : '') + date.day : null;
    }
}
