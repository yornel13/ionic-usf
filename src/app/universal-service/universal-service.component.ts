import { Component, OnInit, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { IonContent } from '@ionic/angular';

@Component({
    selector: 'app-universal-service',
    templateUrl: './universal-service.component.html',
    styleUrls: ['./universal-service.component.scss']
})
export class UniversalServiceComponent implements OnInit {

    @ViewChild('content') ionContent: IonContent;

    request = '';

    constructor(private router: Router) {
        router.events.subscribe(value => {
            if (value instanceof NavigationEnd) {
                this.ionContent.scrollToTop(500).then();
                if (value.url === '/universal-service/personal-dates') {
                    this.request = 'Validación SSN';
                } else if (value.url === '/universal-service/usf-verification') {
                    this.request = 'Información adicional';
                } else if (value.url === '/universal-service/address-date') {
                    this.request = 'Validación de Dirección';
                } else if (value.url === '/universal-service/register-case') {
                    this.request = 'Registro de Solicitud';
                } else if (value.url === '/universal-service/document-digitalization') {
                    this.request = 'Digitalización de documentos';
                } else if (value.url === '/universal-service/account-creation') {
                    this.request = 'Apertura de cuenta';
                } else if (value.url === '/universal-service/activation') {
                    this.request = 'Confirmación';
                } else if (value.url === '/universal-service/social-secure-verification') {
                    this.request = 'Validacion de Prepago';
                } else {
                    this.request = '';
                }

            }
        });
    }

    ngOnInit() {
    }

}
