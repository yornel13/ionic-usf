import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { SharedModule } from '../shared/shared.module';
import { IonicModule } from '@ionic/angular';

@NgModule({
    imports: [CommonModule, ReactiveFormsModule, NgbModule, LoginRoutingModule, SharedModule, IonicModule],
    declarations: [LoginComponent]
})
export class LoginModule {
}
