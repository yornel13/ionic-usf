// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
    API_PATH: 'proccess/procesos2.aspx',
    API_LOG: 'https://www.google.com', // log
    // MOCK_API_PATH: 'https://us-central1-first-project-ad81e.cloudfunctions.net/mock3usfprincipal',
    // MOCK_API_PATH: 'http://localhost:5000/first-project-ad81e/us-central1/mock3usfprincipal',
    MOCK_API_PATH: 'https://us-central1-sam123usf.cloudfunctions.net/mock3usfprincipal', // personal mock
    MOCK_API: true,
    // PROD
    // URL: 'https://lifeline.claropr.com/',
    // DEV
    URL: 'https://wslife00042ws.claroinfo.com/',
    // PROD
    // ONBASE_USERNAME: 'CLARODOCPOP',
    // ONBASE_PASSWORD: '7r3$h7ru1t32!'
    // DEV
    ONBASE_USERNAME: 'DOCCOMPUSER',
    ONBASE_PASSWORD: 'onbase12345',
    VERSION: '1.0.0' // Please update this value for each new web distribution
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
